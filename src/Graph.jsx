// @flow
import React, { useEffect, useRef } from 'react';

import { select } from 'd3';

import {
  updateNode, updateLink, updateGraph, enterLink, enterNode, force, zoomed,
} from './utils/d3Utils';

const width = '100%';
const height = '100%';

type Props = {
  nodes: Object[],
  links: Object[],
}


function Graph({ nodes, links }: Props) {
  const graphRef = useRef();
  const svgRef = useRef();

  useEffect(() => {
    const d3Graph = select(graphRef.current);

    force.on('tick', () => {
      d3Graph.call(updateGraph);
    });

    const svg = select(svgRef.current);
    svg.call(zoomed);
  }, []);

  useEffect(() => {
    const d3Graph = select(graphRef.current);

    const d3Nodes = d3Graph.selectAll('.node')
      .data(nodes, node => node.id);

    d3Nodes.enter().append('g').call(enterNode);
    d3Nodes.exit().remove();
    d3Nodes.call(updateNode);

    const d3Links = d3Graph.selectAll('.link')
      .data(links, link => link.id);

    d3Links.enter().insert('path', '.node').call(enterLink);
    d3Links.exit().remove();
    d3Links.call(updateLink);

    force.nodes([...nodes]).force('link').links([...links]);
    force.alpha(1).restart();
  }, [nodes, links]);

  return (
    <>
      <div
        id="tooltip"
        style={{
          opacity: 0,
          position: 'absolute',
          textAlign: 'center',
          padding: 4,
          fontSize: 12,
          fontWeight: 'bold',
          color: 'white',
          borderRadius: 8,
          pointerEvents: 'none',
          backgroundColor: 'gray',
        }}
      />
      <svg id="svg" width={width} height={height} viewBox="0 0 1000 1000" ref={svgRef}>
        <g id="graph" ref={graphRef} />
      </svg>
    </>
  );
}

export default Graph;
