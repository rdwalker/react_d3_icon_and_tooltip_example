import {
  select,
  forceCenter,
  forceLink,
  forceManyBody,
  forceSimulation,
  drag,
  event,
  forceCollide,
  zoom,
} from 'd3';

const force = forceSimulation()
  .force('collide', forceCollide(10).initialize(10))
  .force('charge', forceManyBody().strength(-30))
  .force('link', forceLink().strength(1).distance(100).iterations(10))
  .force('center', forceCenter(500, 500));

const updateNode = (nodeSelection) => {
  nodeSelection.attr('transform', d => `translate(${d.x} ${d.y})`);
};

const updateLink = (linkSelection) => {
  linkSelection.attr('d', d => `M ${d.source.x} ${d.source.y} L ${d.target.x} ${d.target.y}`);
};

const updateGraph = (graphSelection) => {
  graphSelection.selectAll('.node').call(updateNode);
  graphSelection.selectAll('.link').call(updateLink);
};

const dragStarted = (d) => {
  if (!event.active) {
    force.alphaTarget(0.3).restart();
  }

  force.force('center', null);

  const u = d;
  u.fx = d.x;
  u.fy = d.y;
};

const dragging = (d) => {
  const u = d;
  u.fx = event.x;
  u.fy = event.y;
};

const dragEnded = () => {
  if (!event.active) {
    force.alphaTarget(0);
  }
};

const enterNode = (nodeSelection) => {
  const size = 30;
  const iconSize = 40;

  const tooltip = select('#tooltip');

  nodeSelection
    .on('mouseover', (d) => {
      tooltip.transition()
        .duration(200)
        .style('opacity', 0.8); // show the tooltip
      tooltip.html(d.id)
        .style('left', `${event.pageX}px`)
        .style('top', `${event.pageY}px`);
    })
    .on('mouseleave', () => {
      tooltip.transition()
        .duration(200)
        .style('opacity', 0);
    });

  nodeSelection
    .classed('node', true)
    .append('circle')
    .attr('r', size)
    .attr('stroke', 'black')
    .attr('stroke-width', 1)
    .attr('fill', 'lightgray');

  nodeSelection
    .append('text')
    .classed('material-icons', true)
    .html('computer')
    .style('font-size', iconSize)
    .style('user-select', 'none')
    .attr('x', -iconSize / 2)
    .attr('y', iconSize / 2);

  nodeSelection.call(drag()
    .on('start', dragStarted)
    .on('drag', dragging)
    .on('end', dragEnded));
};

const enterLink = (linkSelection) => {
  linkSelection.classed('link', true)
    .attr('stroke-width', 2)
    .attr('stroke', 'black');
};

const zoomActions = () => {
  select('#graph').attr('transform', event.transform);
};

const zoomed = zoom().on('zoom', zoomActions);

export {
  updateNode, updateLink, updateGraph, enterNode, enterLink, force, zoomed,
};
