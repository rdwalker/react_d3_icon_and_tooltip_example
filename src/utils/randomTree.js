const genRandomTree = (N = 300) => ({
  nodes: [...Array(N).keys()].map(i => ({ id: i })),
  links: [...Array(N).keys()]
    .filter(id => id)
    .map((id, i) => ({
      id: i,
      source: id,
      target: Math.round(Math.random() * (id - 1)),
    })),
});


export default genRandomTree;
