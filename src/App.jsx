// @flow
import React from 'react';

import Graph from './Graph';
import genRandomTree from './utils/randomTree';

const { nodes, links } = genRandomTree(10);

function App() {
  return (
    <div style={{
      height: '100vh',
      width: '100vw',
    }}
    >
      <Graph nodes={nodes} links={links} />
    </div>
  );
}

export default App;
